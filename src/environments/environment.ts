// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

var firebaseConfig = {
  apiKey: "AIzaSyAIBxyhKHXJYdI8QuULxDUQHW1Y7-C4Y8Q",
  authDomain: "covid19cloud-da799.firebaseapp.com",
  databaseURL: "https://covid19cloud-da799.firebaseio.com",
  projectId: "covid19cloud-da799",
  storageBucket: "covid19cloud-da799.appspot.com",
  messagingSenderId: "226048158427",
  appId: "1:226048158427:web:831ce6940fba86cdd7bafe",
  measurementId: "G-RRSZ3H1B70"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
